#!/usr/bin/env python

import boto3

ec2 = boto3.resource('ec2', region_name='eu-west-1')

# User data Code
user_data = '''#!/bin/bash
yum -y install git
git clone https://kdevesh:FY_AtKS_Pjhr3xLLMaye@gitlab.com/deveshkrai/pipeline.git'''

# Create instance
instance = ec2.create_instances(
	ImageId='ami-0fad7378adf284ce0',
	MinCount=1,
	MaxCount=1,
	KeyName='my_keypair_name',
	InstanceType='t2.micro',
	BlockDeviceMappings=[
    	{
        	'DeviceName': '/dev/xvda',
        	'Ebs': {
            	'VolumeSize': 30,
            	'VolumeType': 'standard'
        	}
    	}
	],
    TagSpecifications=[
            {
                'ResourceType': 'instance',
                'Tags': [
                    {
                        'Key': 'Name',
                        'Value': 'eu00elsevier01',
                    },
                    {
                        'Key': 'LinuxDomain',
                        'Value': 'testlab.com',
                    },
                    {
                        'Key': 'Dtap',
                        'Value': 'DTA',
                    },
                    {
                        'Key': 'Owner',
                        'Value': 'Elsevier',
                    },
                ],
            },
        ],
    UserData=user_data,
    NetworkInterfaces=[
        {
           'SubnetId': 'subnet-01b05349',
           'Groups': ['sg-ec15e091'],
           'DeviceIndex': 0,
           'DeleteOnTermination': True,
           'AssociatePublicIpAddress': True,
        }])

for instances in instance:
  print("waiting for the instance to be running state")
  instances.wait_until_running()
  instances.reload()
  print("Instance id ", instances.id)
  print("Instance state ", instances.state['Name'])
  print("Instance public DNS", instances.public_dns_name)
  print("Instance private DNS ", instances.private_dns_name)